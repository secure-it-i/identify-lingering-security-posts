/*
 * Copyright (c) 2017, Venkatesh-Prasad Ranganath
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath
 */


import groovy.xml.XmlUtil

@Grab(group='commons-io', module='commons-io', version='2.5')

import java.nio.file.Paths
import org.apache.commons.io.FilenameUtils
import groovyx.gpars.actor.DynamicDispatchActor

final NUM_PROCS = Runtime.getRuntime().availableProcessors() - 2
final SYNC_LIMIT = 1e6+3  // number of lines to sync between master and workers
final BATCH_SIZE = 1e4  // number of lines in a batch

def cli = new CliBuilder(usage:"groovy convertToCSV.groovy")
cli.i(longOpt:'inputFile', args:1, argName:'inputFile', required:true,
    'One of the XML files from Stack Overflow data dump')
cli.o(longOpt:'outputDir', args:1, argName:'outputDir', required:true,
    'Output directory')
cli.f(longOpt:'fields', args:1, argName:'fields', required:true,
    'Comma-separated fields to include in CSV')
def options = cli.parse(args)
if (!options) {
    return
}

final inputFile = new File(options.i)
final fields = options.f.split(',').collect { "@$it" }
final tmp1 = inputFile.toPath()
final outputFileName = 'lean-' +
    FilenameUtils.removeExtension(tmp1.getFileName().toString()) + '.csv'
final outputFile = Paths.get(options.o ?: '.', outputFileName).toFile()
println("Output file: $outputFile")
println("${new Date()} 0 lines processed")
outputFile.withWriter { writer ->
    writer.println(options.f)

    final collector = new DynamicDispatchActor().become {
        when { List lines -> lines.each(writer.&println) }
        when { boolean i ->
            writer.close()
            terminate()
        }
    }.start()

    final processors = (0..<NUM_PROCS).collect { index ->
        final xmlSlurper = new XmlSlurper()
        new DynamicDispatchActor().become {
            when { List lines ->
                def res = lines.collect { line ->
                        // FIX: handle non-printable Unicode characters
                        def tmp2 = line.replaceAll('[^\\p{Print}]', '?')
                        tmp2 = tmp2.replaceAll(',', ';')
                        def xml = xmlSlurper.parseText(tmp2)
                        def tmp3 = fields.collect { xml[it] ?: '' }.join(',')
                        XmlUtil.escapeControlCharacters(tmp3)
                    }
                collector.send res
            }
            when { String w -> replyIfExists 0 }
            when { boolean i -> terminate() }
        }.start()
    }

    def procNum = 0
    def lines = []
    def prevSyncLineNum = 0
    def reader = inputFile.newReader()
    reader.readLine() // skip xml declaration line
    reader.readLine() // skip <posts> line
    reader.eachLine { line, lineNum ->
        lines << line
        if (lines.size() > BATCH_SIZE) {
            processors[procNum % NUM_PROCS].send lines
            if (lineNum - prevSyncLineNum > SYNC_LIMIT) {
                processors[procNum % NUM_PROCS].sendAndWait 'sync'
                prevSyncLineNum = lineNum
            }
            procNum++
            lines = []
        }
        if (lineNum % 500000 == 0) {
            println("${new Date()} $lineNum lines processed")
        }
    }
    if (lines.size() > 1) {
        processors[procNum % NUM_PROCS].send lines[0..-2] // skip </posts> line
    }
    processors.each { it << false }
    processors*.join()
    collector << false
    collector.join()
}
