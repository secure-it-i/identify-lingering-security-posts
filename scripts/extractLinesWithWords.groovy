/*
 * Copyright (c) 2017, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath
 */


def cli = new CliBuilder(usage:"groovy extractLinesWithWords.groovy")
cli.l(longOpt:'linesFile', args:1, argName:'linesFile', required:true,
    'File to select lines from')
cli.w(longOpt:'wordsFile', args:1, argName:'wordsFile', required:true,
    'File with filtering words')
def options = cli.parse(args)
if (!options) {
    return
}

final words = new HashSet()
new File(options.w).eachLine { words.add(it.trim()) }

new File(options.l).eachLine { line ->
    def tmp1 = line.split(',')
    if (tmp1[0] in words) {
        println(line)
    }
}
