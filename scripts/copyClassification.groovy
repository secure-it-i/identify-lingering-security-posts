/*
 * Copyright (c) 2017, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath
 */

def cli = new CliBuilder(usage:"groovy copyClassification.groovy")
cli.f(longOpt:'from', args:1, argName:'from', required:true,
    'Tags description with classification (as described at the end of step1 ' +
    'in masterScript.sh)')
cli.t(longOpt:'to', args:1, argName:'to', required:true,
    'Tags description (as described at the end of step1 in masterScript.sh)')
def options = cli.parse(args)
if (!options) {
    return
}

println("${new Date()} collecting classification")
def conceptualTags = new HashSet()
def technologicalTags = new HashSet()
new File(options.f).readLines().each { line ->
    def tmp1 = line.split(',')[0].split(' ')
    (tmp1[0] == 'C' ? conceptualTags : technologicalTags) << tmp1[1]
}

println("${new Date()} copying classification")
new File('tags-description-with-classification.txt').withPrintWriter { writer ->
    new File(options.t).readLines().each { line ->
        def tmp1 = line.split(',')[0]
        def tmp2 = ''
        if (tmp1 in conceptualTags) {
            tmp2 = 'C'
        } else if (tmp1 in technologicalTags) {
            tmp2 = 'T'
        }
        writer.println("$tmp2 $line")
    }
}
