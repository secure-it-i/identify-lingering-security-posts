#!/bin/bash
#
# Copyright (c) 2017, Venkatesh-Prasad Ranganath
#
# Licensed under BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath
#

# SECTION: You should modify these entries
SOF_DATA_DIR=/Volumes/Expt_Data/StackOverflow-Jun2017-Data
SSE_DATA_DIR=/Volumes/Expt_Data/SecurityStackExchange-Jun2017-Data

# SECTION: You may modify these entries
DATA_DIR=./data
if ! [ -d $DATA_DIR ] ; then
    mkdir $DATA_DIR
fi

# SECTION: Leave me alone :)
# SoF related
POSTS_XML_FILE=$SOF_DATA_DIR/Posts.xml
LEAN_POSTS_CSV_FILE=$SOF_DATA_DIR/lean-Posts.csv
COMMENTS_XML_FILE=$SOF_DATA_DIR/Comments.xml
LEAN_COMMENTS_CSV_FILE=$SOF_DATA_DIR/lean-Comments.csv
VOTES_XML_FILE=$SOF_DATA_DIR/Votes.xml
LEAN_VOTES_CSV_FILE=$SOF_DATA_DIR/lean-Votes.csv
TAGS_XML_FILE=$SOF_DATA_DIR/Tags.xml

# Security StackExchange related
SSE_TAGS_XML_FILE=$SSE_DATA_DIR/Tags.xml

ANCHOR_TAG=security
MINIMUM_NUM_OF_POSTS=100
TAG_COLUMN_NUMBER_IN_POST_FILE=10
POST_ID_COLUMN_NUMBER_IN_POST_FILE=0
POST_TYPE_ID_COLUMN_NUMBER_IN_POST_FILE=1
PARENT_ID_COLUMN_NUMBER_IN_POST_FILE=3
CREATION_DATE_COLUMN_NUMBER_IN_POSTS_FILE=4
POST_ID_COLUMN_NUMBER_IN_VOTES_OR_COMMENTS_FILE=1
CREATION_DATE_COLUMN_NUMBER_IN_VOTES_OR_COMMENTS_FILE=3

TAG_SYNONYMS_FILE=$DATA_DIR/SoF-approved-tag-synonyms.csv
if ! [ -e $TAG_SYNONYMS_FILE ] ; then
    echo "$TAG_SYNONYMS_FILE does not exist. Please refer to README for instructions to create it"
    exit -1
fi


TAGS_FILE=$DATA_DIR/tags-occurring-with-security-tag.txt
TAG_QUESTIONS_FILE=$DATA_DIR/tags-with-tagged-question-ids.txt
TAGS_FREQ_FILE=$DATA_DIR/tags-frequency.txt
CONCEPTUAL_TAGS_TO_CONSIDER_FILE=$DATA_DIR/conceptual-tags-to-consider.txt
TAGS_COOCCURENCE_FILE=$DATA_DIR/cooccurence-of-tags-with-atleast-$MINIMUM_NUM_OF_POSTS-posts.csv
QUESTION_ANSWERS_FILE=$DATA_DIR/questions-with-answer-ids.txt
QUESTIONS_FILE=$DATA_DIR/questions.txt
ANSWERS_FILE=$DATA_DIR/answers.txt
COMMENTS_BASED_TIME_DATA_FOR_QUESTIONS_FILE=$DATA_DIR/comments-based-time-data-for-questions.txt
COMMENTS_BASED_TIME_DATA_FOR_ANSWERS_FILE=$DATA_DIR/comments-based-time-data-for-answers.txt
VOTES_BASED_TIME_DATA_FOR_QUESTIONS_FILE=$DATA_DIR/votes-based-time-data-for-questions.txt
VOTES_BASED_TIME_DATA_FOR_ANSWERS_FILE=$DATA_DIR/votes-based-time-data-for-answers.txt
TOP_QUESTIONS_FOR_TAGS_FILE=$DATA_DIR/top-questions-for-tags.txt
TOP_QUESTIONS_FOR_CONCEPTUAL_TAGS_FILE=$DATA_DIR/top-questions-for-conceptual-tags.txt
FIVE_NUM_SUMMARY_FOR_TAGS_FILE=$DATA_DIR/five-num-summaries-for-tags.txt
TAGS_DESCRIPTION_FILE=$DATA_DIR/tags-description.txt
TAGS_DESCRIPTION_WITH_CLASSIFICATION_FILE=$DATA_DIR/tags-description-with-classification.txt
RANKED_TAGS_FILE=$DATA_DIR/ranked-tags.txt

SSE_TAGS_FILE=$DATA_DIR/sse_tags.txt

export JAVA_OPTS="-Xmx6144m $JAVA_OPTS"

case $1 in
-step1)
    echo "$(date) convert Posts.xml into CSV file without Body attribute"
    groovy scripts/convertToCSV.groovy \
        -i $POSTS_XML_FILE \
        -o $SOF_DATA_DIR \
        -f "Id,PostTypeId,AcceptedAnswerId,ParentId,CreationDate,Score,"\
"ViewCount,LastEditDate,LastActivityDate,Title,Tags,AnswerCount,"\
"CommentCount,FavoriteCount"


    echo "$(date) collecting tags that occur with security"
    groovy scripts/collectTagsOccurringWithGivenTag.groovy \
        -p $LEAN_POSTS_CSV_FILE \
        -a $ANCHOR_TAG \
        -t $TAG_COLUMN_NUMBER_IN_POST_FILE

    mv $(basename $TAGS_FILE) $TAGS_FILE


    # NOTE: Modify contents TAGS_FILE here to control the considered tags


    echo "$(date) collecting questions with given tags"
    groovy scripts/collectQuestionIdsWithGivenTags.groovy \
        -p $LEAN_POSTS_CSV_FILE \
        -t $TAGS_FILE \
        -q $POST_ID_COLUMN_NUMBER_IN_POST_FILE \
        -a $TAG_COLUMN_NUMBER_IN_POST_FILE
    mv $(basename $TAG_QUESTIONS_FILE) $TAG_QUESTIONS_FILE

    echo "$(date) gathering frequency of tags"
    cat $TAG_QUESTIONS_FILE | \
        cut -f 1,2 -d "," | \
        sort -b -t ',' -k 2 -g \
        > $TAGS_FREQ_FILE


    echo "$(date) gathering answers for given questions"
    groovy scripts/collectAnswerIdsForGivenQuestionIds.groovy \
        -p $LEAN_POSTS_CSV_FILE \
        -q $TAG_QUESTIONS_FILE \
        -a $PARENT_ID_COLUMN_NUMBER_IN_POST_FILE \
        -o $POST_ID_COLUMN_NUMBER_IN_POST_FILE \
        -y $POST_TYPE_ID_COLUMN_NUMBER_IN_POST_FILE
    mv $(basename $QUESTION_ANSWERS_FILE) $QUESTION_ANSWERS_FILE


    echo "$(date) convert Comments.xml into CSV file without Text and User "\
    "related attributes"
    groovy scripts/convertToCSV.groovy \
        -i $COMMENTS_XML_FILE \
        -o $SOF_DATA_DIR \
        -f "Id,PostId,Score,CreationDate"


    echo "$(date) extracting questions"
    cat $QUESTION_ANSWERS_FILE | \
        cut -f 1 -d ',' | \
        sed '1d' \
        > $QUESTIONS_FILE

    echo "$(date) extracting answers"
    cat $QUESTION_ANSWERS_FILE | \
        cut -f 3 -d ',' | \
        tr ' ' '\n' | \
        sed -e '1d' -e '/^$/d' \
        > $ANSWERS_FILE


    echo "$(date) gathering comment based time data for given questions"
    groovy scripts/collectCommentOrVoteBasedTimeDataForGivenPostIds.groovy \
        -c $LEAN_COMMENTS_CSV_FILE \
        -p $POST_ID_COLUMN_NUMBER_IN_VOTES_OR_COMMENTS_FILE \
        -d $CREATION_DATE_COLUMN_NUMBER_IN_VOTES_OR_COMMENTS_FILE \
        -i $QUESTIONS_FILE
    mv comments-based-time-data-for-posts.txt $COMMENTS_BASED_TIME_DATA_FOR_QUESTIONS_FILE

    echo "$(date) gathering comment based time data for given answers"
    groovy scripts/collectCommentOrVoteBasedTimeDataForGivenPostIds.groovy \
        -c $LEAN_COMMENTS_CSV_FILE \
        -p $POST_ID_COLUMN_NUMBER_IN_VOTES_OR_COMMENTS_FILE \
        -d $CREATION_DATE_COLUMN_NUMBER_IN_VOTES_OR_COMMENTS_FILE \
        -i $ANSWERS_FILE
    mv comments-based-time-data-for-posts.txt $COMMENTS_BASED_TIME_DATA_FOR_ANSWERS_FILE


    echo "$(date) convert Votes.xml into CSV file"
    groovy scripts/convertToCSV.groovy \
        -i $VOTES_XML_FILE\
        -o $SOF_DATA_DIR \
        -f "Id,PostId,VoteTypeId,CreationDate"

    sed -e "s/T00:00:00.000//g" $LEAN_VOTES_CSV_FILE \
        > $LEAN_VOTES_CSV_FILE.trim
    mv $LEAN_VOTES_CSV_FILE.trim $LEAN_VOTES_CSV_FILE

    echo "$(date) gathering vote based time data for given questions"
    groovy scripts/collectCommentOrVoteBasedTimeDataForGivenPostIds.groovy \
        -v $LEAN_VOTES_CSV_FILE \
        -p $POST_ID_COLUMN_NUMBER_IN_VOTES_OR_COMMENTS_FILE \
        -d $CREATION_DATE_COLUMN_NUMBER_IN_VOTES_OR_COMMENTS_FILE \
        -i $QUESTIONS_FILE
    mv votes-based-time-data-for-posts.txt $VOTES_BASED_TIME_DATA_FOR_QUESTIONS_FILE

    echo "$(date) gathering vote based time data for given answers"
    groovy scripts/collectCommentOrVoteBasedTimeDataForGivenPostIds.groovy \
        -v $LEAN_VOTES_CSV_FILE \
        -p $POST_ID_COLUMN_NUMBER_IN_VOTES_OR_COMMENTS_FILE \
        -d $CREATION_DATE_COLUMN_NUMBER_IN_VOTES_OR_COMMENTS_FILE \
        -i $ANSWERS_FILE
    mv votes-based-time-data-for-posts.txt $VOTES_BASED_TIME_DATA_FOR_ANSWERS_FILE


    echo "$(date) identify top questions for tags based time data"
    groovy scripts/identifyTopQuestionsBasedOnTimeData.groovy \
        -t $TAG_QUESTIONS_FILE \
        -p $LEAN_POSTS_CSV_FILE \
        -i $POST_ID_COLUMN_NUMBER_IN_POST_FILE \
        -c $CREATION_DATE_COLUMN_NUMBER_IN_POSTS_FILE \
        -a $PARENT_ID_COLUMN_NUMBER_IN_POST_FILE \
        -q $QUESTION_ANSWERS_FILE \
        -o $COMMENTS_BASED_TIME_DATA_FOR_QUESTIONS_FILE \
        -m $COMMENTS_BASED_TIME_DATA_FOR_ANSWERS_FILE \
        -v $VOTES_BASED_TIME_DATA_FOR_QUESTIONS_FILE \
        -e $VOTES_BASED_TIME_DATA_FOR_ANSWERS_FILE
    mv $(basename $TOP_QUESTIONS_FOR_TAGS_FILE) $TOP_QUESTIONS_FOR_TAGS_FILE


    echo "$(date) gathering tags co-occurrence frequency"
    groovy scripts/calculateTagCooccurrenceFrequency.groovy \
        -p $LEAN_POSTS_CSV_FILE  \
        -t $TAGS_FREQ_FILE \
        -a $TAG_COLUMN_NUMBER_IN_POST_FILE \
        -m $MINIMUM_NUM_OF_POSTS
    mv $(basename $TAGS_COOCCURENCE_FILE) $TAGS_COOCCURENCE_FILE


    echo "$(date) calculate five-num summary for questions"
    groovy scripts/calculateFiveNumSummaryOfNumericFeaturesForGivenTags.groovy \
        -p $LEAN_POSTS_CSV_FILE \
        -t $TAGS_FILE \
        -o $POST_TYPE_ID_COLUMN_NUMBER_IN_POST_FILE \
        -a $TAG_COLUMN_NUMBER_IN_POST_FILE \
        -f 5,6,11,12,13
    mv $(basename $FIVE_NUM_SUMMARY_FOR_TAGS_FILE) $FIVE_NUM_SUMMARY_FOR_TAGS_FILE


    echo "$(date) extract tag description"
    groovy scripts/extractTagDescription.groovy \
        -l $TAGS_FILE \
        -t $TAGS_XML_FILE \
        -p $POSTS_XML_FILE
    mv $(basename $TAGS_DESCRIPTION_FILE) $TAGS_DESCRIPTION_FILE


    echo "Now, "
    echo "1) copy $TAGS_DESCRIPTION_FILE as $TAGS_DESCRIPTION_WITH_CLASSIFICATION_FILE."
    echo "2) classify tags in $TAGS_DESCRIPTION_WITH_CLASSIFICATION_FILE as" \
         "conceptual (prefix: 'C ') or technological (prefix: 'T ')" \
         "by adding the prefix to each line."
    echo "3) execute scripts/masterScript.sh -step2."
    ;;

-step2)
    if ! [ -e $TAGS_DESCRIPTION_WITH_CLASSIFICATION_FILE ]  ; then
        echo "Please execute scripts/masterScript.sh -step1, follow the" \
             "instructions at the end, and then execute scripts/masterScript.sh step2"
        exit 1
    fi
    echo "$(date) extract conceptual tags"
    cat $TAGS_DESCRIPTION_WITH_CLASSIFICATION_FILE | \
        egrep "^C " | \
        sed -e "s/^C //" | \
        cut -f1 -d',' \
        > $CONCEPTUAL_TAGS_TO_CONSIDER_FILE


    echo "$(date) extract top questions for given conceptual tags"
    groovy scripts/extractLinesWithWords.groovy \
        -l $TOP_QUESTIONS_FOR_TAGS_FILE \
        -w $CONCEPTUAL_TAGS_TO_CONSIDER_FILE \
        > $TOP_QUESTIONS_FOR_CONCEPTUAL_TAGS_FILE


    echo "$(date) extract tags from security.stackexchange"
    cat $SSE_TAGS_XML_FILE | \
        sed -E 's/.*TagName="([^"]+)".*/\1/' | \
        grep -v ">" |
        sort -u \
        > $SSE_TAGS_FILE


    echo "$(date) rank SoF tags considering tags from security.stackexchange"
    groovy scripts/rankTags.groovy \
        -c $CONCEPTUAL_TAGS_TO_CONSIDER_FILE \
        -t $TAGS_FILE \
        -o $TAGS_COOCCURENCE_FILE \
        -s $SSE_TAGS_FILE \
        -a $ANCHOR_TAG \
        -y $TAG_SYNONYMS_FILE \
        -f 50 \
        > $RANKED_TAGS_FILE
    ;;

*)
    echo "usage: scripts/masterscript.sh [-step1|-step2]"
esac
