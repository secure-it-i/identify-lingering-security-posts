/*
 * Copyright (c) 2017, Venkatesh-Prasad Ranganath
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath
 */

def cli = new CliBuilder(usage:"groovy getTopMQuestionsForTopNTags.groovy")
cli.q(longOpt:'questions', args:1, argName:'questions', required:true,
    'Top questions for each tag (generated from identifyTopQuestionsBasedOnTimeData.groovy)')
cli.m(longOpt:'numOfQues', args:1, argName:'numOfQues', required:true,
    'Num of top questions to consider')
cli.t(longOpt:'tags', args:1, argName:'tags', required:true,
    'Ranked list of tags (generated from rankTags.groovy)')
cli.n(longOpt:'numOfTags', args:1, argName:'numOfTags', required:true,
    'Num of top tags to consider')
def options = cli.parse(args)
if (!options) {
    return
}

final M = options.m as int
final N = options.n as int
final tag2questions = new LinkedHashMap()
new File(options.t).readLines().take(N).each { tag2questions[it] = [] }
final reader = new File(options.q).newReader()
reader.readLine()
reader.eachLine { line ->
    def tmp1 = line.split(',')
    def tag = tmp1[0]
    if (tag2questions.containsKey(tag)) {
        tag2questions[tag].addAll(
                tmp1[1].split(' ')
                       .take(M)
                       .collect { it.split(':')[0] })
    }
}

final filename = "top-$M-questions-for-top-$N-tags.txt"
final questions = new HashSet()
new File(filename).withWriter { writer ->
    tag2questions.each { k, v ->
        v.findAll { !(it in questions) }
         .each {
            writer.println(it)
            questions.add(it)
        }
    }
}


