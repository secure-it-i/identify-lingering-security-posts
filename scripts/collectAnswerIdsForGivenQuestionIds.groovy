/*
 * Copyright (c) 2017, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath
 */


import groovy.transform.ToString
import groovyx.gpars.actor.DynamicDispatchActor

final NUM_PROCS = Runtime.getRuntime().availableProcessors() - 2
final SYNC_LIMIT = 1e6+3  // number of lines to sync between master and slaves
final BATCH_SIZE = 1e4  // number of lines in a batch

@ToString
final class IntPair {
    Integer fst, snd
}

def cli = new CliBuilder(usage:"groovy collectAnswerIdsForGivenQuestionIds.groovy")
cli.p(longOpt:'postsFile', args:1, argName:'postsFile', required:true,
    'Posts csv file generated by convertToCSV.groovy script')
cli.q(longOpt:'tagQuestionFile', args:1, argName:'tagQuestionFile', required:true,
    'Tag to question ids file generated by collectQuestionIdsWithGivenTags.groovy')
cli.a(longOpt:'parentIdColNum', args:1, argName:'parentIdColNum', required:true,
    '0-based parent id column number')
cli.o(longOpt:'postIdColNum', args:1, argName:'postIdColNum', required:true,
    '0-based post id column number')
cli.y(longOpt:'postTypeColNum', args:1, argName:'postTypeColNum', required:true,
    '0-based post type column number')
def options = cli.parse(args)
if (!options) {
    return
}

final parentIdColNum = options.a as int
final postIdColNum = options.o as int
final postTypeColNum = options.y as int

println("Reading tag questions file ${new Date()}")
final post2answers = [:]
def reader = new File(options.tagQuestionFile).newReader()
reader.readLine()
reader.eachLine { line ->
    line.split(',')[2].trim().split(' ')
        .findAll { !(post2answers.containsKey(it as int)) }
        .each { post2answers[it as int] = [] }
}

final collector = new DynamicDispatchActor().become {
    when { List pairs -> pairs.each { post2answers[it.fst] << it.snd } }
    when { boolean f -> terminate() }
}.start()

final processors = (0..<NUM_PROCS).collect {
    new DynamicDispatchActor().become {
        when { List lines ->
            def res = lines.collect { it.split(',') }
                           .findAll {
                                it[postTypeColNum] == '2' &&
                                post2answers.containsKey(it[parentIdColNum] as int) }
                           .collect {
                                new IntPair(
                                    fst: it[parentIdColNum] as int,
                                    snd: it[postIdColNum] as int)
                           }
            collector << res
        }
        when { String b -> replyIfExists 0 }
        when { boolean b -> terminate() }
    }.start()
}

println("${new Date()} 0 lines processed")
def procNum = 0
def lines = []
def prevSyncLineNum = 0
reader = new File(options.postsFile).newReader()
reader.readLine() // skip header
reader.eachLine { line, lineNum ->
    lines << line
    if (lines.size() > BATCH_SIZE) {
        processors[procNum % NUM_PROCS].send lines
        if (lineNum - prevSyncLineNum > SYNC_LIMIT) {
            processors[procNum % NUM_PROCS].sendAndWait 'sync'
            prevSyncLineNum = lineNum
        }
        procNum++
        lines = []
    }
    if (lineNum % 500000 == 0) {
        println("${new Date()} $lineNum lines processed")
    }
}
processors[procNum % NUM_PROCS].send lines
processors.each { it << false }
processors*.join()
collector << false
collector.join()

new File("questions-with-answer-ids.txt").withPrintWriter { writer ->
    writer.println("Question Post Id,# of Answer Posts,PostIds")
    post2answers.each { k, v ->
        writer.println("$k,${v.size()},${v.join(' ')}")
    }
}
