/*
 * Copyright (c) 2017, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath
 */


import groovy.transform.TypeChecked
import groovyx.gpars.actor.DynamicDispatchActor

final NUM_PROCS = Runtime.getRuntime().availableProcessors() - 2
final SYNC_LIMIT = 1e6+3  // number of lines to sync between master and slaves
final BATCH_SIZE = 1e4  // number of lines in a batch

// Quantile calculation is based on the Remedian method generalized to
// different quartiles
// http://web.ipac.caltech.edu/staff/fmasci/home/astro_refs/Remedian.pdf

@TypeChecked
class QuantileEstimator {
    private final static int LAYER_LENGTH = 1999
    private List<List<Integer>> layers = (1..3).collect { [] }
    float quantile = 0.5

    private int getQuantileIndexForLayer(int layerIndex) {
        def layer = layers[layerIndex]
        Math.min(Math.max(layer.size() * quantile as int, 0), layer.size() - 1)
    }


    def add(int value) {
        addValueToLayer(value, 0)
    }

    def getEstimate() {
        for (i in 3..0) {
            if (layers[i]) {
                def tmp1 = getQuantileIndexForLayer(i)
                return layers[i].sort()[tmp1]
            }
        }
        return null
    }

    @TypeChecked
    private addValueToLayer(int value, int layer) {
        if (layers[layer].size() < LAYER_LENGTH) {
            layers[layer] << (value as int)
        } else {
            int tmp1 = getQuantileIndexForLayer(layer)
            addValueToLayer(layers[layer].sort()[tmp1], layer + 1)
            layers[layer].clear()
            layers[layer] << (value as int)
        }
    }
}

@TypeChecked
class FiveNumsEstimator {
    private List<QuantileEstimator> fivenums = [0.0, 0.25, 0.5, 0.75, 1.0].collect {
        new QuantileEstimator(quantile: it)
    }

    def getEstimates() {
        return fivenums.collect { it.getEstimate() }
    }

    def add(int value) {
        fivenums.each { it.add(value) }
    }
}

def cli = new CliBuilder(usage:"groovy calculateFiveNumSummaryOfNumericFeaturesForGivenTags.groovy")
cli.p(longOpt:'postsFile', args:1, argName:'postsFile', required:true,
    'Posts csv file generated by convertToCSV.groovy script')
cli.t(longOpt:'tagFile', args:1, argName:'tagFile', required:true,
    'Tag to consider')
cli.o(longOpt:'postTypeIdColNum', args:1, argName:'postTypeIdColNum', required:true,
    '0-based post type id column number')
cli.a(longOpt:'tagsColNum', args:1, argName:'tagsColNum', required:true,
    '0-based tags column number')
cli.f(longOpt:'featureColNums', args:1, argName:'featureColNums', required:true,
    'Comma-separated list of 0-based column number of freq features')
def options = cli.parse(args)
if (!options) {
    return
}

println("${new Date()} Reading questions and tags")
final postTypeIdColNum = options.o as int
final tagsColNum = options.a as int
final featureColumns = options.f.split(',').collect { it as int }
final tag2feature2fivenums = [:]
def reader = new File(options.t).newReader()
reader.eachLine { tag ->
    tag2feature2fivenums[tag] = featureColumns.inject([:]) { acc, c ->
        acc[c] = new FiveNumsEstimator() ; acc
    }
}

final processors = (0..<NUM_PROCS).collect {
    new DynamicDispatchActor().become {
        when { List lines ->
            lines.collect { it.split(',', 14)[postTypeIdColNum,
                                              tagsColNum,
                                              featureColumns] }
                 .findAll { it[0] == '1' }
                 .each {
                    def tags = it[1].replaceAll('><', ',')[1..-2]
                         .split(',')
                         .findAll(tag2feature2fivenums.&containsKey)
                    tags.each { tag ->
                        def tmp1 = tag2feature2fivenums[tag]
                        synchronized (tmp1) {
                            featureColumns.eachWithIndex { col, idx ->
                                tmp1[col].add((it[idx + 2] ?: 0) as int)
                            }
                        }
                     }
                 }
        }
        when { String s -> replyIfExists 0 }
        when { boolean b -> terminate() }
    }.start()
}

println("${new Date()} 0 lines processed")
def procNum = 0
def lines = []
def prevSyncLineNum = 0
reader = new File(options.p).newReader()
final featureNames = reader.readLine().split(',')[featureColumns]
reader.eachLine { line, lineNum ->
    lines << line
    if (lines.size() > BATCH_SIZE) {
        processors[procNum % NUM_PROCS].send lines
        if (lineNum - prevSyncLineNum > SYNC_LIMIT) {
            processors[procNum % NUM_PROCS].sendAndWait 'sync'
            prevSyncLineNum = lineNum
        }
        procNum++
        lines = []
    }
    if (lineNum % 500000 == 0) {
        println("${new Date()} $lineNum lines processed")
    }
}
processors[procNum % NUM_PROCS].send lines
processors.each { it << false }
processors*.join()

new File("five-num-summaries-for-tags.txt").withPrintWriter { writer ->
    def tmp1 = ['Tags']
    featureNames.each {
        tmp1 << "$it-min"
        tmp1 << "$it-lower_quartile"
        tmp1 << "$it-median"
        tmp1 << "$it-upper_quartile"
        tmp1 << "$it-max"
    }
    writer.println(tmp1.join(','))
    tag2feature2fivenums.each { k, v ->
        def tmp2 = [k]
        featureColumns.each { tmp2.addAll(v[it].getEstimates()) }
        writer.println(tmp2.join(','))
    }
}
