/*
 * Copyright (c) 2017, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath
 */

import groovyx.gpars.actor.DynamicDispatchActor

final NUM_PROCS = Runtime.getRuntime().availableProcessors() - 2
final SYNC_LIMIT = 1e6+3  // number of lines to sync between master and slaves
final BATCH_SIZE = 1e2  // number of lines in a batch

def cli = new CliBuilder(usage:"groovy extractTagDescription.groovy")
cli.l(longOpt:'tagList', args:1, argName:'tagList', required:true,
    'File of tags (one per line)')
cli.p(longOpt:'postsFile', args:1, argName:'postsFile', required:true,
    'Posts xml file available as part of Stack Overflow data dump')
cli.t(longOpt:'tagsFile', args:1, argName:'tagsFile', required:true,
    'Tags xml file available as part of Stack Overflow data dump')
def options = cli.parse(args)
if (!options) {
    return
}

println("${new Date()} reading tag list")
final tags = new File(options.l).readLines().collect { it.trim() }

println("${new Date()} reading tags file")
final postId2tag = [:]
// We will only consider tags with description,
// i.e., ExcerptPostId attribute is present
def tagPattern = ~/.*<row.*TagName="([^"]+).*ExcerptPostId="([^"]+)".*/
new File(options.t).newReader().eachLine { line ->
    def tmp1 = line.replaceAll('[^\\p{Print}]', '?')
    def matcher = tagPattern.matcher(tmp1)
    if (matcher) {
        def tagName = matcher.group(1)
        if (tagName in tags) {
            def postId = matcher.group(2)
            postId2tag[postId] = tagName
        }
    }
}
println("${postId2tag.size()} tags found with desription")

final writer = new File('tags-description.txt').newPrintWriter()
final collector = new DynamicDispatchActor().become {
    when { List lines -> lines.each(writer.&println) }
    when { boolean f -> 
        writer.close() 
        terminate() 
    }
}.start()

final postTypeIds = (3..5).collect { "PostTypeId=\"$it\"".toString() }
final processors = (0..<NUM_PROCS).collect {
    new DynamicDispatchActor().become {
        when { List lines ->
            def res = lines.collect { it.replaceAll('[^\\p{Print}]', '?') }
                           .findAll { it.contains("<row") &&
                                it.trim().split(' ')[2] in postTypeIds }
                           .inject([]) { acc, line ->
                                def slurper = new XmlSlurper()
                                def tmp1 = slurper.parseText(line)
                                def postId = (tmp1.@Id).text()
                                if (postId in postId2tag) {
                                    def tmp2 = tmp1.@Body.text()
                                    tmp2 = tmp2.replaceAll('\r', ' ')
                                    tmp2 = tmp2.replaceAll('\n', ' ')
                                    acc << "${postId2tag[postId]},${tmp2}"
                                }
                                acc
                            }
            if (res) {
                collector << res
            }
        }
        when { String b -> replyIfExists 0 }
        when { boolean b -> terminate() }
    }.start()
}

println("${new Date()} 0 lines processed")
def procNum = 0
def lines = []
def prevSyncLineNum = 0
new File(options.p).newReader().eachLine { line, lineNum ->
    lines << line
    if (lines.size() > BATCH_SIZE) {
        processors[procNum % NUM_PROCS].send lines
        if (lineNum - prevSyncLineNum > SYNC_LIMIT) {
            processors[procNum % NUM_PROCS].sendAndWait 'sync'
            prevSyncLineNum = lineNum
        }
        procNum++
        lines = []
    }
    if (lineNum % 500000 == 0) {
        println("${new Date()} $lineNum lines processed")
    }
}
processors[procNum % NUM_PROCS].send lines
processors.each { it << false }
processors*.join()
collector << false
collector.join()

