/*
 * Copyright (c) 2017, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath
 */

import groovyx.gpars.actor.DynamicDispatchActor

final NUM_PROCS = Runtime.getRuntime().availableProcessors() - 2
final SYNC_LIMIT = 1e6+3  // number of lines to sync between master and workers
final BATCH_SIZE = 1e4  // number of lines in a batch

def cli = new CliBuilder(
    usage:"groovy collectTagsOccurringWithGivenTag.groovy")
cli.p(longOpt:'postsFile', args:1, argName:'postsFile', required:true,
    'Posts csv file')
cli.a(longOpt:'anchorTag', args:1, argName:'anchorTag', required:true,
    'Tag with which other tags should co-occur')
cli.t(longOpt:'tagsColNum', args:1, argName:'tagsColNum', required:true,
    '0-based tags column number')
def options = cli.parse(args)
if (!options) {
    return
}

final anchorTag = options.anchorTag
final tagsColNum = options.tagsColNum as int

final coOccurringTags = new HashSet()
final collector = new DynamicDispatchActor().become {
    when { Set tags -> coOccurringTags.addAll(tags) }
    when { boolean i -> terminate() }
}.start()

// Only posts of type 1 have Tags.
final tagExtractionPattern = ~/([^"]*)<${anchorTag}>([^"]*)/
final processors = (0..<NUM_PROCS).collect { index ->
    new DynamicDispatchActor().become {
        when { List lines ->
            def res = lines.collect {
                    tagExtractionPattern.matcher(it.split(',')[tagsColNum])
                }
                .findAll { it.matches() }
                .collect { it.group(1) + it.group(2) }
                .findAll { it }
                .collectMany {
                    it.replaceAll('><', ',')[1..-2]
                      .split(',') as List
                } as Set
            collector.send res
        }
        when { String s -> replyIfExists 0 }
        when { boolean i -> terminate() }
    }.start()
}

println("${new Date()} 0 lines processed ${coOccurringTags.size()} ")
def procNum = 0
def lines = []
def prevSyncLineNum = 0
def reader = new File(options.postsFile).newReader()
reader.readLine() // skip header line
reader.eachLine { line, lineNum ->
    lines << line
    if (lines.size() > BATCH_SIZE) {
        processors[procNum % NUM_PROCS].send lines
        if (lineNum - prevSyncLineNum > SYNC_LIMIT) {
            processors[procNum % NUM_PROCS].sendAndWait 'sync'
            prevSyncLineNum = lineNum
        }
        procNum++
        lines = []
    }
    if (lineNum % 500000 == 0) {
        println("${new Date()} $lineNum lines processed \
${coOccurringTags.size()}")
    }
}
processors[procNum % NUM_PROCS].send lines
processors.each { it << false }
processors*.join()
collector << false
collector.join()

final fileName = "tags-occurring-with-${anchorTag}-tag.txt"
new File(fileName).withPrintWriter { writer ->
    coOccurringTags << anchorTag
    coOccurringTags.sort().each { writer.println(it) }
}
