This repository contains scripts to process _Stack Overflow data dump_ available as part of [Stack Exchange Data Dumps](https://archive.org/details/stackexchange) to identify lingering questions pertaining to a topic; typically identified by a tag such as *security*.  The schema for the data dump is available [here](https://meta.stackexchange.com/questions/2677/database-schema-documentation-for-the-public-data-dump-and-sede#2678).

## Requirements

- A computer with
    - at least 4 cores and
    - sufficient RAM to dedicate 6GB of RAM to a JVM.
    - at least 5GB of storage space
- [JDK 8+](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Groovy 2.4.11+](http://groovy-lang.org/)

On Unix, use [sdkman](http://sdkman.io/) to get groovy and jdk. On Windows based system, use Powershell as the command-line shell and get groovy via [posh-gvm](https://github.com/flofreud/posh-gvm).

## Execution

- Download posts, comments, tags, and votes files from _Stack Overflow_ data dump and unzip them in a folder, e.g., SOF.
- Download _Security.StackExchange_ data dump file and unzip it in a folder, e.g., SSE.
- Assign the absolute path of SOF folder to `SOF_DATA_DIR` variable in `scripts/masterScript.sh`.
- Assign the absolute path of SSE folder to `SSE_DATA_DIR` variable in `scripts/masterScript.sh`.
- Execute `select SourceTagName, TargetTagName from TagSynonyms where ApprovedByUserId <> 0` at [Stack Exchange Data Explorer](https://data.stackexchange.com/stackoverflow/query/new) and download the resulting CSV file as **$DATA_DIR/SoF-approved-tag-synonyms.csv**.  We downloaded this information on Jun-20-2017.
- [Optional] Modify `DATA_DIR` variable to be a folder where you want to store the data generated by the scripts.
- Run
    - On Unix: `scripts/masterScript.sh -step1` in the root folder of the repository clone.
    - **On Windows: `scripts/masterScript.ps1 -step1` in the root folder of the repository clone. [TBD]**
- The execution will generate the following files in `DATA_DIR` folder.
    - **answers.txt**: Answer post ids considered to determine the top questions.
    - **comments-based-time-data-for-answers.txt:** Answer post ids along with number of associated comments and the time of the first and the last comment .
    - **comments-based-time-data-for-questions.txt:** Question post ids along with number of associated comments and the time of the first and the last comment.
    - **cooccurence-of-tags-with-atleast-100-posts.csv:** Tag cooccurrencece data in terms of tag names, number of common questions, and both absolute and relative frequencies.
    - **five-num-summaries-for-tags.txt:** (Approximate) Five number summary of _score, view count, answer count, comment count,_ and _favourite count_ for each tag.
    - **questions-with-answer-ids.txt:** Question post ids along with number of answers and the answer post ids.
    - **questions.txt:** Question post ids considered to determine the top questions.
    - **tags-description.txt:** Tags along with their description.
    - **tags-frequency.txt:** Frequency of each tag in the data dump.
    - **tags-occurring-with-security-tag.txt:** Tags occuring with _security_ tag.
    - **tags-with-tagged-question-ids.txt:** Tags along with number of questions for each tag and the post ids of the questions.
    - **top-questions-for-tags.txt:** Top questions identified based on how long the question has been active (based on any activity related to a question, its answers, its comments, comments to its answers, and votes to the question and its answers) along with the longevity of the question (number of days between the post and the last activity) and its recentness (time of last activity).
    - **votes-based-time-data-for-answers.txt:** Answer post ids along with number of associated votes and the time of the first and the last votes.
    - **votes-based-time-data-for-questions.txt:** Question post ids along with number of associated votes and the time of the first and the last votes.
- Edit a copy of _tags-description.txt_ to classify tags as conceptual (prefix: "C ") or technological (prefix: "T ") by adding the prefix to each line and save it as **$DATA_DIR/tags-description-with-classification.txt**.
- Run
    - On Unix: `scripts/masterScript.sh -step2` in the root folder of the repository clone.
    - **On Windows: `scripts/masterScript.ps1 -step2` in the root folder of the repository clone. [TBD]**
- The execution will generate the following files in `DATA_DIR` folder.
    - **conceptual-tags-to-consider.txt:** Tags to consider while picking questions for analysis.
    - **ranked-tags.txt:** Tags ranked in decreasing order for analysis.
    - **sse-tags.txt:** Tags found in _security.stackexchange.com_ as of Jun-21-2017.
    - **top-questions-for-conceptual-tags.txt:** A copy of _top-questions-for-tags.txt_ limited only to conceptual tags.

## Notes

- Time data is measured as the number of days since _2008-01-01_.


## Attribution

Copyright (c) 2017, Kansas State University

Licensed under BSD 3-clause "New" or "Revised" License (https://choosealicense.com/licenses/bsd-3-clause/)

**Authors:** Venkatesh-Prasad Ranganath
