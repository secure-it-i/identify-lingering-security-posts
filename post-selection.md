1. Automated Step 1:
  1. Gather the set of tags T that co-occur with 'security' tag
  2. Gather the set of questions Q with tags in T
  3. Calculate frequency of tags in T
  4. Gather the set of answers A to questions in Q
  5. Gather comment based time data for questions in Q
  6. Gather comment based time data for questions in A
  7. Gather vote based time data for questions in Q
  8. Gather vote based time data for questions in A
  9. For each question in Q, identify the dates when the first and the last 
     comments were posted. 
  10. For each answer in A, identify the dates when the first and the last 
      comments were posted.
  11. For each question in Q, identify the dates when the first and the last 
      votes were posted.
  12. For each answer in A, identify the dates when the first and the last 
      votes were posted.
  13. For each tag in T, sort/rank the questions in ascending order.
    1. Using the data from steps 9-12, identify the age (in days) of a question 
       based the first activity and the last activity.  An activity is 
       - posting of the question, an answer to the question, or a comment to 
         the question or to one of its answers
       - votign for the question or an answer to the question
    2. Question q1 is ranked higher than question q2 if q1 is older than q2,
       i.e., q1.age > q2.age
    3. When questions are of of the same age, then the question is with more 
       recent last activity is ranked higher.
  10. Calculate co-occurrence frequency for tags in T that are associated
      with at least in 100 posts.
  11. Gather tag description for tags in T
2. Manual Step 1: Using tag description collected in step 1.11, manually 
   classify tags in T as being either
  - conceptual, i.e., independent of any specific technology 
  - technology-specific
3. Automated Step 2:
  1. Gather top questions TQ for the identified conceptual tags CT
  2. Gather set of tags ST from security.stackexchange.com (SSE)
  3. Rank tags in CT 
    1. Consider only tags that co-occur with 'security' tag
    2. Calculate the # of top questions for each tag
    3. Scale the # of questions associated with a tag by 50 if the tag occurs in ST
    4. Rank the tags based on the number of associated questions

Now consider the top M questions of the top N tags.
